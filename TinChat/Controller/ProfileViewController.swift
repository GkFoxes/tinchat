//
//  ProfileViewController.swift
//  TinChat
//
//  Created by Дмитрий Матвеенко on 21/09/2018.
//  Copyright © 2018 GkFoxes. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton! {
        didSet {
            editButton.layer.borderWidth = 2
            editButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    // MARK: - View Controller Life Cycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        /* print(editButton.frame)
        
        Не работает - (Thread 1: Fatal error: Unexpectedly found nil while unwrapping an Optional value).
        Ошибка показывает нам, что невозможно сейчас распечатать frame кнопки editButton, т.к в данный момент значение editButton == none (nil).
        В этом методе обычно выделяют ресурсы, которые нужны будут view controller и на данном этапе метод не имеет представление о editButton, тем более о его frame. */
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(editButton.frame)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print(editButton.frame)
        /* Frame кнопки изменился, т.к. метод viewDidLoad() вызывается до того, как определены границы в методе viewDidLayoutSubviews().
        Там мы вызываем метод layoutSubviews(), который программно рассчитывает frames и после уже показывается на экране. */
    }
    
    // MARK: - Show Message Function
    
    func showMessage(userMessage: String) -> Void {
        let alertController = UIAlertController(title: "Что-то пошло не так!", message: userMessage, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action: UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Button Action
    
    @IBAction func setProfileImage(_ sender: Any) {
        print("Выбери изображение профиля")
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Выберите способ загрузки", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                self.showMessage(userMessage: "Камера недоступна.")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                self.showMessage(userMessage: "Галерея недоступна.")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - Image Picker Functions
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        profileImageView.image = image
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
